//This program generates a pseudo-random number, then converts it to an actual card in a standard deck
//Program by Sam Friedman
public class CardGenerator{
  public static void main(String[] args){
    int randomCard=(int)((Math.random()*52)+1);
    String cardNumber="";
    String cardSuit="";
    if(randomCard<14){
      cardSuit="Diamonds";
    }else if(randomCard>=14&&randomCard<27){
      cardSuit="Clubs";
    }else if(randomCard>=27&&randomCard<40){
      cardSuit="Hearts";
    }else if(randomCard>=30){
      cardSuit="Spades";
    }
    switch (randomCard%13){
      case 0:
        cardNumber="King";
        break;
      case 1:
        cardNumber="Ace";
        break;
      case 11:
        cardNumber="Jack";
        break;
      case 12:
        cardNumber="Queen";
        break;
      default:
        cardNumber+=(randomCard%13);
        break;
    }
    System.out.println("You picked the "+cardNumber+" of "+cardSuit);
  }
}