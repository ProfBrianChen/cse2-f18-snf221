import java.util.Scanner;
public class LabFive{
  public static void main (String[] args){
    Scanner myScanner= new Scanner(System.in);
    String departmentName="";
    int courseNumber=0;
    int meetingsPerWeek=0;
    String startTime="";
    String professorName="";
    int numStudents=0;
    while(true){
      System.out.print("Enter the course number: ");
      if(myScanner.hasNextInt()){
        courseNumber=myScanner.nextInt();
        break;
      }else{
        System.out.println("Error: expected an integer, please enter an integer.");
        myScanner.next();
      }
    }
    while(true){
      System.out.print("Enter the department name : ");
      if(myScanner.hasNextLine()){
        departmentName=myScanner.next();
        break;
      }else{
        System.out.println("Error: expected a string, please enter a string.");
        myScanner.next();
      }
    }
    while(true){
      System.out.print("Enter the number of meetings per week: ");
      if(myScanner.hasNextInt()){
        meetingsPerWeek=myScanner.nextInt();
        break;
      }else{
        System.out.println("Error: expected an integer, please enter an integer.");
        myScanner.next();
      }
    }
    while(true){
      System.out.print("Enter the class start time : ");
      if(myScanner.hasNextLine()){
        startTime=myScanner.next();
        break;
      }else{
        System.out.println("Error: expected a string, please enter a string.");
        myScanner.next();
      }
    }    
    while(true){
      System.out.print("Enter the professor's name : ");
      if(myScanner.hasNextLine()){
        professorName=myScanner.next();
        break;
      }else{
        System.out.println("Error: expected a string, please enter a string.");
        myScanner.next();
      }
    }
    while(true){
      System.out.print("Enter the number of students: ");
      if(myScanner.hasNextInt()){
        numStudents=myScanner.nextInt();
        break;
      }else{
        System.out.println("Error: expected an integer, please enter an integer.");
        myScanner.next();
      }
    }
    System.out.println("Your class is: "+departmentName+" "+courseNumber+" which meets "+meetingsPerWeek+" time(s) a week at "+startTime+" with Professor "+professorName+" and "+numStudents+" student(s)");
  }
}