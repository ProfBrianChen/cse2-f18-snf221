// This program takes the inputs of affected area in acres and rainfall in inches to give the result of total rainfall in cubic miles.
// Program by Sam Friedman
import java.util.Scanner; //Importing the Scanner class in order to use it in the program
public class Convert{
  public static void main(String[] args){ // The main method required for every java program
    final double GALLONS_PER_INCH_PER_ACRE = 27154; //This number is the one used by the USGS, found on google
    final double GALLONS_PER_CUBIC_MILE = 1.101117e12; //This number is from the google online unit converter
    // I set up the conversion calculations as final because they are constant and shouldn't change
    Scanner myScanner=new Scanner(System.in);//Creating an instance of Scanner to recieve input
    System.out.print("Enter the affected area in acres: ");
    double acres = myScanner.nextDouble(); //We want to use a double because partial acres are acceptable
    System.out.print("Enter the rainfall in the affected area: ");
    double rainfall=myScanner.nextDouble(); // As with acres, it is also possible to have fractional inches of rainfall
    double cubicMiles=((acres*rainfall*GALLONS_PER_INCH_PER_ACRE)/GALLONS_PER_CUBIC_MILE);//The numerator of this equation is the total number of gallons of rainfall, the denominator is the number of gallons in a cubic mile, giving the result of the number of cubic miles of rainfall
    System.out.println(cubicMiles + " cubic miles"); //Finally, the number of cubic miles of rain is returned
  }
}