public class WelcomeClass{
  public static void main(String[] args){
/*
This program is designed to output the welcome message included this comment in addtion to a short biographical statement.
  -----------
  | WELCOME |
  -----------
  ^  ^  ^  ^  ^  ^
 / \/ \/ \/ \/ \/ \
<-S--N--F--2--2--1->
 \ /\ /\ /\ /\ /\ /
  v  v  v  v  v  v
*/
    // The first print statement contains the first three lines of the welcome message.
    System.out.println("-----------\n| WELCOME |\n-----------");
    // The second print statement contains the last five lines of the welcome message.
    System.out.println("  ^  ^  ^  ^  ^  ^\n / \\/ \\/ \\/ \\/ \\/ \\ \n<-S--N--F--2--2--1->\n \\ /\\ /\\ /\\ /\\ /\\ / \n  v  v  v  v  v  v");
    // The last print statement contains my "tweet-length" autobiographical satement.
    System.out.println("I've been to two child psychologists and they still don't know what's wrong with me.");
  }
}