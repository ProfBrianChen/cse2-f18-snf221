//This program takes the inputs of a check total, a desired tip percentage, and the number of people in a group to calculate even payment among all group members.
import java.util.Scanner; //We import the scanner class in order to use in in the program.
public class Check{
  public static void main(String[] args){
    Scanner myScanner=new Scanner(System.in); //Since we have imported the scanner, we can now initialize it
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    double checkCost=myScanner.nextDouble(); // We sent the value of checkCost to the next double input, as we have given an informative prompt on the number we are looking for and the format we want it written in.
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble(); // we use the scanner again to assign value to tipPercent, agian giving a prompt to ensure proper format is used. Because it is a double, tip percentages with decimal values will also be calculated correctly.
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt(); //We use an integer for this instance of the scanner because we expect the number of people in a party to be a whole number.
    double totalCost;
    double costPerPerson;
    int dollars,   //whole dollar amount of cost 
    dimes, pennies; //for storing digits
                   //to the right of the decimal point 
                   //for the cost$ 
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople; //The costPerPerson value is likely to be a very long decimal, but we only need the first 2 decimal places for a dollar value.
    //get the whole amount, dropping decimal fraction
    dollars=(int)costPerPerson;
    //get dimes amount, e.g., 
    // (int)(6.73 * 10) % 10 -> 67 % 10 -> 7
    //  where the % (mod) operator returns the remainder
    //  after the division:   583%100 -> 83, 27%5 -> 2 
    dimes=(int)(costPerPerson * 10) % 10; // By moving the decimal place and taking the % operator, we get the remainder value which is equal to the dimes and pennies based on how many decimal places we move.
    pennies=(int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);//Finally, we output the final cost per person. Note that the values of dollars, dimes, and pennies are incorporated in a string and is not a single number.
  }
}