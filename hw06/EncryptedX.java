//This program generates a square with an encrypted X in the negative space
//Program by Sam Friedman
import java.util.Scanner; //Importing the scanner class
public class EncryptedX{
  public static void main(String [] args){ //Declaring a class and main method
    Scanner scan=new Scanner(System.in); //Creating a new scanner to use in the program
    System.out.print("Please enter an integer between 0 and 100: ");
    int flagSize=-1; //The default value for flagSize is an invalid number to make sure that the program doesn't run at 0 accidentally
    while(flagSize<0||flagSize>100){ // This while loop keeps running until a valid input is given
      if(scan.hasNextInt()){
          flagSize=scan.nextInt();
        if(flagSize<0||flagSize>100){
          System.out.print("Error: invalid input. Please enter an integer between 0 and 100: ");
        }
      }else{
        System.out.print("Error: invalid input. Please enter an integer between 0 and 100: ");
        scan.next();
      }

  }
    
    for(int i=0;i<flagSize+1;i++){ //These nested for loops build the encrypted X by printing spaces on the diagonal lines
      for(int j=0;j<flagSize+1;j++){
        if(j==i||j==flagSize-i){
          System.out.print(" ");
        }else{
          System.out.print("*");
        }
      }
      System.out.println();
    }
}
}