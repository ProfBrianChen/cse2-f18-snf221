// This program takes the values of trip times, wheel revolution counts, the size of the wheel, and some constants to compute and output
// the trip times, total revolutions in the trip, the distance of each trip in miles and the total disttance of the trips in miles
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      int secsTrip1=480;  // The number of seconds for trip #1, in this case 480 seconds (8 minutes)
      int secsTrip2=3220;  // The number of seconds for trip #2, in this case 3220 seconds (53 minutes 40 seconds)
      int countsTrip1=1561;  // The number of wheel revolutions counted during trip 1
      int countsTrip2=9037; // The Number of wheel revolutions counted during trip 2
      double wheelDiameter=27.0;  // The diameter of the bike wheel in inches
      double PI=3.14159; // Setting up the value of pi as a variable for ease of use later
      int feetPerMile=5280;  // Setting the number of feet in a mile as a variable for ease of use later
      int inchesPerFoot=12;   // Setting the number of inches in a foot as a variable for ease of use later
      double secondsPerMinute=60;  // Setting the number of seconds in a minute as a variable for ease of use later
      double distanceTrip1, distanceTrip2,totalDistance;  // These variables don't have values yet, but they are being initialized now so they can be used later
      System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+" minutes and had "+ countsTrip1+" counts.");
      System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+ countsTrip2+" counts.");// These two print statements output the number of minutes and revolutions in coherent sentences, making it easier for the user to understand
      distanceTrip1=countsTrip1*wheelDiameter*PI; 
      /* By multiplying the wheel diameter times pi, we get the circumfrence of the wheel. 
      If we then multiply that by the number of revolutions in a trip, 
      the value of distanceTrip1 will be equal to the length of the trip in inches. */
      distanceTrip1/=inchesPerFoot*feetPerMile; // This converts the value of distanceTrip1 from inches to its equivalent value in miles
      distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // This line combines the previous two statements into one statement, but for distanceTrip2
      totalDistance=distanceTrip1+distanceTrip2; // This adds the two trip distances together as sets the result as the value for totalDistance
      System.out.println("Trip 1 was "+distanceTrip1+" miles");
	    System.out.println("Trip 2 was "+distanceTrip2+" miles");
	    System.out.println("The total distance was "+totalDistance+" miles"); 
      // These three lines above output the values that were just calculated (distanceTrip1, distanceTrip2, totalDistance)
	}  //end of main method   
} //end of class