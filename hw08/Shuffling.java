import java.util.Scanner; //importing scanner
public class Shuffling{ 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
String[] suitNames={"C","H","S","D"};    
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
String[] cards = new String[52]; //This array will hold all the cards in the deck
String[] hand = new String[5]; //This array holds the hand, and the length is the number of cards in a hand
int numCards = hand.length; //if the size of the hand array changes, the number of cards requested in getHand also increases
int again = 1; // The variable for requesting an additional hand
int index = 51;
for (int i=0; i<52; i++){ //This loop constructs the array in order
  cards[i]=rankNames[i%13]+suitNames[i/13];
} 
printArray(cards); 
cards=shuffle(cards); 
printArray(cards); 
  
while(again == 1){
   if(index<numCards){
     index=51;
     cards=shuffle(cards);
     printArray(cards);
  }
   hand = getHand(cards,index,numCards);
   System.out.println("Hand");
   printArray(hand);
   index = index - numCards;
   System.out.print("Enter a 1 if you want another hand drawn: "); 
   again = scan.nextInt(); 
}  
  }

public static void printArray(String[] list){//This function iterates through the array and prints out each value
  for(int i=0;i<list.length;i++){
    System.out.print(list[i]+" ");
  }
  System.out.println();
}
public static String[] shuffle(String[] list){//This function swaps the values of the first item and a random item
  System.out.println("Shuffled");
  for (int i=0;i<100;i++){
    int random=(int)((Math.random()*51)+1);
    String placeholder=list[0];
    list[0]=list[random];
    list[random]=placeholder;
  }
  return list;
}
public static String[] getHand(String[] cards,int index,int numCards){ //This function generates hands
  String[] hand=new String[numCards];
  for(int i=0;i<hand.length;i++){
    hand[i]=cards[index-i];
  }
  return hand;
}
}