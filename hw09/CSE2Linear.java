//This program takes inputs of 15 integers for final CSE2 grades, and allows for searching for grades using both binary and linear searches based on user inputs
//Program by Sam Friedman
import java.util.Arrays;//Importing Arrays to use the toString property to allow easier printing of arrays
import java.util.Scanner;
import java.util.Random;//Scanner and Random will both be used later in the program
public class CSE2Linear{
    public static void main(String[] args){
        Scanner scan=new Scanner(System.in);//Creating a scanner to handle user input
        int[] grades=new int[15];//Creating an array to hold the grade values
        System.out.print("Please enter 15 ascending ints for final grades in CSE2: ");
        for(int i=0;i<grades.length;){//This for loop will keep running until 15 acceptable inputs have been entered
            if(scan.hasNextInt()){
                int input=scan.nextInt();
                if(i==0||grades[i-1]<=input){
                    if(input>=0&&input<=100) {
                        grades[i] = input;
                        i++;//This is the only place where i increments, making sure that only acceptable inputs are counted
                    }else{
                        System.out.println("The number must be bwtween 1-100, inclusive.");
                    }
                }else{
                    System.out.println("The integers need to be entered in ascending order.");
                }
            }else{
                scan.next();
                System.out.println("Please enter an integer.");
            }
        }
        System.out.println(Arrays.toString(grades));
        System.out.print("Enter a grade to search for: ");//The completed array is printed out and the user is prompted to enter a value to search for
        int search=scan.nextInt();
        binary(search, grades);//First, a binary search is run on the sorted array
        System.out.println("Scrambled:");
        shuffle(grades);//Then, another method randomizes the order of the array, making binary search ineffective
        System.out.println(Arrays.toString(grades));
        System.out.print("Enter a grade to search for: ");
        search=scan.nextInt();
        linear(search,grades);//Based on another inputted integer, a linear search is performed
    }
    public static void binary(int search, int[] grades){//This method implements a binary search
        int high=grades.length;
        int low=0;
        for(int i=0;i<Math.pow(grades.length,0.5);){
            i++;
            int mid=(int)((high+low)/2);
            if(grades[mid]==search){
                System.out.println(search+" was found in "+i+" iterations.");//The program will notify the user how many times the loop ran prior to finding the value
                break;
            }else if(grades[mid]<search){
                low=mid+1;
            }else if(grades[mid]>search){
                high=mid-1;
            }
            if(i>(int)Math.pow(grades.length,0.5)){
                System.out.println(search+" not found in "+i+" iterations.");//If the value is not found, the number of times the loop ran is still output
                break;
            }
        }
    }
    public static void shuffle(int[] grades){//This method swaps the first value of the array with a random one 25 times to create a randomized array order
        Random rand=new Random();
        int placeholder=0;
        for(int i=0;i<25;i++){
            int swap=rand.nextInt(grades.length);
            placeholder=grades[0];
            grades[0]=grades[swap];
            grades[swap]=placeholder;
        }
    }
    public static void linear(int search, int[] grades){//a linear search simply compares every value in the array to the target, so it generally takes more iterations than a binary search
        boolean found=false;
        for(int i=0;i<grades.length;i++){
            if(grades[i]==search){
                found=true;
                System.out.println(search+" found in "+(i+1)+" iterations");
                break;
            }
        }
        if(!found){
            System.out.println(search+" not found in "+ grades.length+" iterations.");
        }
    }
}