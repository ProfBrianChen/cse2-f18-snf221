//This program takes the inputs of the side length and height of a square pyramid, and returns the internal volume of the pyramid
//Program by Sam Friedman
import java.util.Scanner; //Importing the Scanner class to use it later in the program
public class Pyramid{
  public static void main(String[] args){ // The main method necessary in every java program
    Scanner myScanner=new Scanner(System.in); // Creating an instance of Scanner to use in this program
    System.out.print("The square side of the pyramid is (input length): ");
    double length=myScanner.nextDouble(); // Taking the input of length and storing it as a double, meaning fractional numbers will work
    System.out.print("The height of the pyramid is (input height): ");
    double height=myScanner.nextDouble();
    double volume=((Math.pow(length,2)*height)/3);//The formula for the volume of a pyramid is (1/3)*area of the base*height, where the area of the base is length^2 in this case, as it is a square pyramid
    System.out.println("The volume inside the pyramid is: "+volume); //Finally, the volume of the pyramid is returned
  }
}