// This program asks the user for a number of poker hands, randomly generates that many hands, and gives the probability of one-pair, two-pair, three-of-a-kind and four-of-a-kind.
// Program by Sam Friedman
import java.util.Scanner; //Importing the scanner class
public class Hw05{
  public static void main(String[] args){
    Scanner scan=new Scanner(System.in); // Creating a new scanner
    System.out.print("How many hands would you like to generate?: ");
    int numHands=0;
    while(true) { // This loop will keep asking for user input until an integer is entered.
    if(scan.hasNextInt()) {
    	 numHands=scan.nextInt();
    	 break;
    }else {
    	scan.next();
    	System.out.print("Error: Expecting an integer, please enter an integer: "); //This will prompt the user to enter an integer if they enter another data type
    }
    }
    int card1=0,card2=0,card3=0,card4=0,card5=0,handsCounter=0;
    int pairs=0,twoPair=0,threeKind=0, fourKind=0; //Initializing the values for all of the cards and the total number of how many times each hand type occurs. They are out of the loop to make sure they are in scope for multiple loops
    while(handsCounter<numHands){ //this loop generates poker hands, making sure that it doesn't draw the same card from the deck twice. It loops using handsCounter as a counter variable to count up to the number of hands requested
      int numCards=0;
      while(numCards<5){ // We want to generate a hand of five cards, which is achieved in this loop
        int cardID=(int)(Math.random()*52)+1;
        switch(numCards){
          case 0:
            card1=cardID;
            numCards++;
            break;
          case 1:
            if(cardID!=card1){
              card2=cardID;
              numCards++;
            }
            break;
          case 2:
            if(cardID!=card1&&cardID!=card2){
              card3=cardID;
              numCards++;
            }
            break;
          case 3:
            if(cardID!=card1&&cardID!=card2&&cardID!=card3){
              card4=cardID;
              numCards++;
            }
            break;
          case 4:
            if(cardID!=card1&&cardID!=card2&&cardID!=card3&&cardID!=card4){ //Every time we generate a new card, we want to make sure that it isn't a card that was already drawn
              card5=cardID;
              numCards++;
            }
            break;
        }

      }
        int cardVal1=card1%13,cardVal2=card2%13,cardVal3=card3%13,cardVal4=card4%13,cardVal5=card5%13; // Because the hands in question regard the value and not the suit, we take the mod of each card so they are being compared by their face value
        int mostSame=0;
      //Here's where I'd put my array... IF I COULD USE ONE
/*        int cardVals[]={card1%13,card2%13,card3%13,card4%13,card5%13};
          for(int i=0;i<cardVals.length;i++){
          for (int j=0;j<cardVals.length;j++){
            if(i!=j){
              if(cardVals[i]==cardVals[j]){
                mostSame++;  
              }
            }
          }
        }*/
      if(cardVal1==cardVal2){ //In this series of if statements, the value of mostSame is incremented twice for each matching pair of cards, giving the variable a value of 2 for a pair, 4 for a two-pair, 6 for a three-of-a-kind, 8 for a full house, and 12 for a four-of-a-kind
        mostSame++;
      }
      if(cardVal1==cardVal3){
        mostSame++;
      }
      if(cardVal1==cardVal4){
        mostSame++;
      }
      if(cardVal1==cardVal5){
        mostSame++;
      }
      if(cardVal2==cardVal1){
        mostSame++;
      }
      if(cardVal2==cardVal3){
        mostSame++;
      }
      if(cardVal2==cardVal4){
        mostSame++;
      }
      if(cardVal2==cardVal5){
        mostSame++;
      }
      if(cardVal3==cardVal1){
        mostSame++;
      }
      if(cardVal3==cardVal2){
        mostSame++;
      }
      if(cardVal3==cardVal4){
        mostSame++;
      }
      if(cardVal3==cardVal5){
        mostSame++;
      }
      if(cardVal4==cardVal1){
        mostSame++;
      }
      if(cardVal4==cardVal2){
        mostSame++;
      }
      if(cardVal4==cardVal3){
        mostSame++;
      }
      if(cardVal4==cardVal5){
        mostSame++;
      }
      if(cardVal5==cardVal1){
        mostSame++;
      }
      if(cardVal5==cardVal2){
        mostSame++;
      }
      if(cardVal5==cardVal3){
        mostSame++;
      }
      if(cardVal5==cardVal4){
        mostSame++;
      }
      switch (mostSame){ //based on the value of mostSame, the counter of the proper hand type is incremented
        case 2:
          pairs++;
          break;
        case 4:
          twoPair++;
          break;
        case 6:
          threeKind++;
          break;
        case 8:
          pairs++;
          threeKind++;
          break;
        case 12:
          fourKind++;
      }
       handsCounter++; //Finally, the program increments by 1 and generates another hand

    }
    System.out.println("The number of loops: "+numHands);
    System.out.printf("The probability of Four-of-a-kind: %1.3f\n", (double)fourKind/numHands);
    System.out.printf("The probability of Three-of-a-kind: %1.3f\n", (double)threeKind/numHands);
    System.out.printf("The probability of Two-Pair: %1.3f\n", (double)twoPair/numHands);
    System.out.printf("The probability of One-Pair: %1.3f\n", (double)pairs/numHands); // All of these are printf statements in order to format the values to three decimal places
  }
}