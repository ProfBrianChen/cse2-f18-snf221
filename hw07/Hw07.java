// This program uses methods to perform a variety of operations on a user-inputted sample text based on the user's choice
//Program by Sam Friedman
import java.util.Scanner; //Importing the scanner class
public class Hw07{
  public static void main(String [] args){ // The main method calls the rest of the methods in the program and outputs their return values
    Scanner scan=new Scanner(System.in);
    String sampleText=sampleText();
    boolean qPressed=false; //This boolean is set so the program will break out of the for loop once it is set to true
    while (qPressed==false){
    char selection=printMenu();

    switch(selection){ //Upon recieving a user input, the main method uses a switch statement to call the proper method
      case 'c':
    System.out.println("Number of non-whitespace characters: "+getNumOfNonWSCharacters(sampleText)); //The String sampleText is passed to the method so it can be operated on
        break;
      case 'f':
        System.out.print("Enter a word or phrase to be found:\n");
        String request=scan.nextLine(); //the findText method requires more input, which is asked for and then also passed to the proper method
        System.out.println("\"more\" instances: "+findText(request,sampleText));
        break;
      case 'q':
        qPressed=true;
        break;
      case 'w':
        System.out.println("Number of words: "+getNumOfWords(sampleText));
        break;
      case 'r':
        System.out.println("Edited text: "+replaceExclamation(sampleText));
        break;
      case 's':
        System.out.println("Edited text: "+shortenSpace(sampleText));
        break;
      default: //In the event that the user enters something that isn't a command, this message will display prompting them to select a proper menu option
        System.out.println("Error: Unrecognized character/command, please try again.");
    }
    }    
  } 
  public static int findText(String rq, String st){//This method counts the number of characters that are the same within the length of the requested text to be found. If the number of similar characters are equal to the total number of characters, then it is a match.
   int matches=0;
    for(int i=0;i<st.length()-rq.length();i++){
      int matchingLetters=0;
      for(int j=0;j<rq.length();j++){
        if(rq.charAt(j)==st.charAt(i+j)){
           matchingLetters++;
      }
    }
      if(matchingLetters==rq.length()){
          matches++;
       }
           
  }
    return matches; // Finally, the number of matches is returned to the main method.
}
  public static int getNumOfWords(String st){//This loop counts the number of non-consecutive whitespaces to determine the number of words.
    int wordCounter=0;
    for(int i=0;i<st.length();i++){
      if((st.charAt(i)==' '&&st.charAt(i+1)!=' ')||i==st.length()-1){ //because the last word in a sentence does not have a space after it, the last word gets added to the count due to the last argument of this if statement
        wordCounter++;
      }
    }
    
    return wordCounter; //Finally, the total word count is returned
  }
  public static String sampleText(){ //This method reads a whole line of inputted text from the user then returns it to the main method
    Scanner scan=new Scanner(System.in);
    System.out.print("Please enter a sample text:\n");
    String sampleText=scan.nextLine();
    System.out.println("You entered: "+sampleText);
    return sampleText;
  }
  public static char printMenu(){// This method displays the list of menu options then recieves user input. The input is then returned to the main method
    Scanner scan=new Scanner(System.in);
    System.out.print("MENU\nc - number of non-whitespace characters\nw - number of words\nf - find text\nr - replace all !'s\ns - shorten spaces\nq - quit\n\nChoose an option: ");
    char choice = scan.nextLine().charAt(0);
    return choice;

  }
  public static int getNumOfNonWSCharacters(String st){ //This method gets the total number of non-whitespace characters and returns it to the main method
    int numNonWSCharacters=0;
    for(int i=0;i<st.length();i++){
      char letter=st.charAt(i);
     if(letter!=' '&&letter!='	'){//If a character in the string is a "space" or a "tab" it is not counted towards the total number of non-whitespace characters
      numNonWSCharacters++;
     }
    }
    return numNonWSCharacters;
  }
  public static String replaceExclamation(String st){// This method iterates through the entire sample text and changes exclamation points to periods and stores everything in a new string, which is then returned to the main method
    String editedText="";
    for(int i=0;i<st.length();i++){
      if(st.charAt(i)=='!'){
        editedText+='.';
      }else{
        editedText+=st.charAt(i);
      }
    }
    return editedText;
  }
  public static String shortenSpace(String st){//This method also iterates through the entire string and stores everything except spaces with another space after it, thus removing all but one space between words
    String editedText="";
    for(int i=0;i<st.length();i++){
      if(st.charAt(i)==' '&&st.charAt(i+1)==' '){
        continue;
      }else{
        editedText+=st.charAt(i);
      }
    }
    return editedText;
  }
}