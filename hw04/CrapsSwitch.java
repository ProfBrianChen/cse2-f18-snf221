// This program takes inputted or randomly generated dice values and converts the roll to its cooresponding craps slang term using only case statements
// Program by Sam Friedman
import java.util.Scanner;//Importing scanner to use in the program
public class CrapsSwitch{
  public static void main(String[] args){//The main method needed for every java program
    Scanner myScanner=new Scanner(System.in);//Creating a scanner to use in the main method
    int die1=0;
    int die2=0;
    int decision=0;//Initializing the variables for the values of the two dice and also the decision variable which is used for the user's decision for random or chosen dice rolls
    boolean inputsValid=false;//Initializing a boolean to use in the while loop
    System.out.println("Do you want to randomly generate dice or input values?");
    while(inputsValid==false){//We only want the values of 1 or 2 to advance the program, so the code repeats until one of the options is selected
    System.out.print("Enter 1 for random generation or 2 to input your own values: ");
    decision=myScanner.nextInt();
      switch(decision){
        case 1://Decision 1 causes 2 random integers between 1 and 6 to be generated
          inputsValid=true;
          die1=(int)(Math.random()*6)+1;
          die2=(int)(Math.random()*6)+1;
          break;
        case 2://decision 2 asks for 2 integers to be input for the dice values
          inputsValid=true;
          boolean diceValid=false;
          System.out.println("Please note that if an invalid number is entered, the program will ask for both dice again."); //This is to remind the user to input valid numbers, and explain any extra prompts from the program
          while(diceValid==false){
            System.out.print("Please enter the value of the first die (between 1 and 6, inclusive): ");
            die1=myScanner.nextInt();
            System.out.print("Please enter the value of the second die (between 1 and 6, inclusive): ");
            die2=myScanner.nextInt();
            diceValid=(die1>=1&&die1<=6&&die2>=1&&die2<=6);// This statement checks if the dice values are valid, and changes the state of the boolean to break the loop if they are
      }
          break;
        default:
          break;
      }
    }
      System.out.println("Your dice are: "+die1+" + "+die2);
      int diceID=(die1*10)+die2;//The diceID variable gives each roll a unique number, and always puts die1 before die2
      //The switch statement relies on fallthrough, such that every diceID case that has the same slang term falls through until the proper slang term is printed, upon which the program breaks
      switch(diceID){ //Absolute unit. In awe at the size of this lad. 
        case 11:
       System.out.println("That's Snake Eyes!");
          break;
        case 12:
        case 21:
        System.out.println("That's an Ace Deuce!");
          break;
        case 13:
        case 31:
        System.out.println("That's an Easy Four!");
          break;
        case 14:
        case 41:
        case 23:
        case 32:
          System.out.println("That's a Fever Five!");
          break;
        case 15:
        case 51:
        case 42:
        case 24:
        System.out.println("That's an Easy Six!");
          break;
        case 16:
        case 61:
        case 25:
        case 52:
        case 34:
        case 43:
        System.out.println("That's a Seven Out, better luck next time!");
          break;
        case 22:
        System.out.println("That's a Hard Four!");
          break;
        case 26:
        case 62:
        case 35:
        case 53:
        System.out.println("That's an Easy Eight!");
          break;
        case 33:
        System.out.println("That's a Hard Six!");
          break;
        case 36:
        case 63:
        case 45:
        case 54:
        System.out.println("That's a Nine!");
          break;
        case 44:
        System.out.println("That's a Hard Eight!");
          break;
        case 46:
        case 64:
        System.out.println("That's an Easy Ten!");
          break;
        case 55:
        System.out.println("That's a Hard Ten!");
          break;
        case 65:
        case 56:
        System.out.println("That's a Yo-leven!");
          break;
        case 66:
        System.out.println("That's Boxcars!");
          break;
      }// Every diceID is accounted for.
      }
    }
