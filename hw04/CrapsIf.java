// This program takes inputted or randomly generated dice values and converts the roll to its cooresponding craps slang term using only if statements
// Program by Sam Friedman
import java.util.Scanner; //Importing scanner to use in the program
public class CrapsIf{
  public static void main(String[] args){ //The main method needed for every java program
    Scanner myScanner=new Scanner(System.in); //Creating a scanner to use in the main method
    int die1=0;
    int die2=0;
    int decision=0; //Initializing the variables for the values of the two dice and also the decision variable which is used for the user's decision for random or chosen dice rolls
    boolean inputsValid=false; //Initializing a boolean to use in the while loop
    System.out.println("Do you want to randomly generate dice or input values?");
    while(inputsValid==false){
    System.out.print("Enter 1 for random generation or 2 to input your own values: ");
    decision=myScanner.nextInt();
    if (decision==1||decision==2){ //We only want the values of 1 or 2 to advance the program, so the code repeats until one of the options is selected
      inputsValid=true;
    }
    }
    if(decision==1){ //Decision 1 causes 2 random integers between 1 and 6 to be generated
      die1=(int)(Math.random()*6)+1;
      die2=(int)(Math.random()*6)+1;
    }else if(decision==2){ //decision 2 asks for 2 integers to be input for the dice values 
      boolean diceValid=false;
      while(diceValid==false){ //We set up this boolean and while loop in order to check that the numbers that were input are between 1 and 6, and asks for the numbers again if they aren't valid.
      System.out.print("Please enter the value of the first die (between 1 and 6, inclusive): ");
      die1=myScanner.nextInt();
      System.out.print("Please enter the value of the second die (between 1 and 6, inclusive): ");
      die2=myScanner.nextInt();
      if(die1>=1&&die1<=6&&die2>=1&&die2<=6){ // This statement checks if the dice values are valid, and changes the state of the boolean to break the loop if they are
        diceValid=true;
      }else{
        System.out.println("Those numbers don't look quite right, try again!"); //If one of the dice is invalid, the program prompts the user with this message to input new numbers
      }
      }
    }
      System.out.println("Your dice are: "+die1+" + "+die2);
      int diceID=(die1*10)+die2; //The diceID variable gives each roll a unique number, and always puts die1 before die2
      if(diceID==11){ // Each argument in this series of if/else statements contains all of the diceIDs that coorespond to one slang term, 
       System.out.println("That's Snake Eyes!");
      }else if(diceID==12||diceID==21){
        System.out.println("That's an Ace Deuce!");
      }else if(diceID==13||diceID==31){
        System.out.println("That's an Easy Four!");
      }else if(diceID==14||diceID==41||diceID==23||diceID==32){
        System.out.println("That's a Fever Five!");
      }else if(diceID==15||diceID==51||diceID==42||diceID==24){
        System.out.println("That's an Easy Six!");
      }else if(die1+die2==7){
        System.out.println("That's a Seven Out, better luck next time!");
      }else if(diceID==22){
        System.out.println("That's a Hard Four!");
      }else if(diceID==26||diceID==62||diceID==35||diceID==53){
        System.out.println("That's an Easy Eight!");
      }else if(diceID==33){
        System.out.println("That's a Hard Six!");
      }else if(diceID==36||diceID==63||diceID==45||diceID==54){
        System.out.println("That's a Nine!");
      }else if(diceID==44){
        System.out.println("That's a Hard Eight!");
      }else if(diceID==46||diceID==64){
        System.out.println("That's an Easy Ten!");
      }else if(diceID==55){
        System.out.println("That's a Hard Ten!");
      }else if(diceID==65||diceID==56){
        System.out.println("That's a Yo-leven!");
      }else if(diceID==66){
        System.out.println("That's Boxcars!");
      }// Every diceID is accounted for. Because every roll of 7 has the same slang term, the argument for Seven out is slightly different
    
    }

  }
