//This program takes clothing prices, numbers of items purchased, and the state sales tax rate to compute total and tax total values for each item and all items combined
public class Arithmetic{
  public static void main(String[] args){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    //The total cost for all pairs of pants
    double pantsTotal=numPants*pantsPrice;
    //The total cost for all shirts
    double shirtsTotal=numShirts*shirtPrice;
    //The total cost of all belts
    double beltsTotal=numBelts*beltCost;
    //The cost of tax on all belts
    double beltTax=beltsTotal*paSalesTax;
    //The cost of tax on all pants
    double pantTax=pantsTotal*paSalesTax;
    //The cost of tax on all shirts
    double shirtTax=shirtsTotal*paSalesTax;
    
    //The next block of code takes the values and runs them through the removeFractionalPennies method to round each value to the nearest whole cent and outputs it
    pantsTotal=removeFractionalPennies(pantsTotal);
    System.out.println("Total price of all pants (before tax): $"+pantsTotal);
    pantTax=removeFractionalPennies(pantTax);
    System.out.println("Price of tax on pants: $"+pantTax);
    shirtsTotal=removeFractionalPennies(shirtsTotal);
    System.out.println("Total price of all shirts (before tax): $"+shirtsTotal);
    shirtTax=removeFractionalPennies(shirtTax);
    System.out.println("Price of tax on shirts: $"+shirtTax);
    beltsTotal=removeFractionalPennies(beltsTotal);
    System.out.println("Total price of all belts (before tax): $"+beltsTotal);
    beltTax=removeFractionalPennies(beltTax);
    System.out.println("Price of tax on belts: $"+beltTax);
    //These variables for the final total values are calculated after the clothing prices have been rounded and corrected to maintain accuracy
    double subTotal=pantsTotal+shirtsTotal+beltsTotal; //The total of all clothing before tax
    double taxTotal=beltTax+shirtTax+pantTax; //The total tax for all clothing
    double grandTotal=subTotal+taxTotal; //The total of all clothing plus tax
    //Finally, the total values are output
    System.out.println("Subtotal: $"+subTotal);
    System.out. println("Tax total: $"+taxTotal);
    System.out.println("Total: $"+grandTotal);

  }
  //This method takes an input value and rounds it to the nearest hundredth. It is for turning the earlier computed values into realistic dollar amounts
  public static double removeFractionalPennies(double value){
    value+=0.005; //In adding 0.005, the value will round correctly. Since casting to an int doesn't round inherently, any values that would not increase into the next decimal place will be truncated and not rounded
    value*=100; //Multiply by 100 to retain the values of the first 2 decimal places before casting to an int
    value=(int) value; //value is now an integer with the cents in the ones and tens place
    value/=100; //value is divided by 100 to be retuned to its proper decimal form
    return value; //value is returned to the main method
  }
}