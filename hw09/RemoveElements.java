//This program randomly generates an array of integers, then creates new arrays removing elements based on user input
//Program by Sam Friedman
import java.util.Random;

import java.util.Scanner;//Importing random and scanner to use later in the program

public class RemoveElements {
    public static void main(String [] args){
        Scanner scan=new Scanner(System.in);//Creating a new scanner to handle system input
        int num[]=new int[10];//This is the original array which will contain the randomly generated numbers
        int newArray1[];
        int newArray2[];
        int index,target;
        String answer="";
        do{
            System.out.print("Random input 10 ints [0-9]");
            num = randomInput(); //This runs the randomInput method, which returns 10 digits
            String out = "The original array is:";
            out += listArray(num);
            System.out.println(out);

            System.out.print("Enter the index ");
            index = scan.nextInt();
            newArray1 = delete(num,index);
            String out1="The output array is ";
            out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"
            System.out.println(out1);

            System.out.print("Enter the target value ");
            target = scan.nextInt();
            newArray2 = remove(num,target);
            String out2="The output array is ";
            out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer=scan.next();
        }while(answer.equals("Y") || answer.equals("y"));//The loop will always run once, and will run again if the user enters a "y" or "Y"
    }

    public static String listArray(int num[]){//This method prints an array passed to it and formats it for easy reading
        String out="{";
        for(int j=0;j<num.length;j++){
            if(j>0){
                out+=", ";
            }
            out+=num[j];
        }
        out+="} ";
        return out;
    }
    public static int[] randomInput(){//This method generates an array of 10 integers between 0-9, inclusive
        int[] randomInts=new int[10];
        Random rand=new Random();
        for(int i=0;i<randomInts.length;i++){
            randomInts[i]=rand.nextInt(10);
        }
        return randomInts;
    }
    public static int[] delete(int[] list  ,int pos){
        int[] copy=new int[list.length];

        {
            for(int i=0;i<list.length;i++){
                copy[i]=list[i];
            }
        }
        if(pos<list.length&&pos>=0) {//This loop adds the values to the new array, but only if they aren't the requested position to remove
           int[] deleted=new int[list.length-1];
            int counter=0;
            for (int i = 0; i < list.length; i++) {
                if (i != pos) {
                    deleted[counter] = list[i];
                    counter++;
                }

            }
            return deleted;
        }else{
            System.out.println("The index is not valid.");
            return copy;
        }


    }
    public static int[] remove(int[] list, int target){
        int targetCounter=0;
        for(int i=0;i<list.length;i++){//this loop determines how many time the selected value appears
            if(list[i]==target){
                targetCounter++;
            }
        }
        int[] removed=new int[list.length-targetCounter];//targetCounter is subtracted from list.length to create an array of the proper size
        int arrCounter=0;
        for(int i=0;i<list.length;i++){
            if(list[i]!=target){
                removed[arrCounter]=list[i];
                arrCounter++;
            }
        }
        return removed;
    }

}

